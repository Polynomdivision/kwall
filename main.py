import sys
from pydbus import SessionBus

MIDDLE_SEP = "├"
BOTTOM_SEP = "└"
VERT_SEP = "│"
BAR_SEP = "─"

def get_kwalletd5():
    """
    Opens a Dbus connection to the kwallet daemon

    Returns:
      The Dbus connection
    """
    bus = SessionBus()
    return bus.get("org.kde.kwalletd5", "/modules/kwalletd5")


def print_folder(wallet_id, bus, folder_name, tabs=0):
    """
    Prints the contents of a folder

    Args:
      @walletId: The ID of the opened wallet
      @bus: The session bus of the dbus connection
      @folder_name: The name of the folder
    """
    entry_list = bus.entryList(wallet_id,
                               folder_name,
                               "kde-service-menu-nowardev-scanner")

    # List all element of all
    if tabs == 0:
        print("[{}]".format(folder_name))
    else:
        print(MIDDLE_SEP + 2*BAR_SEP + " " + folder_name)

    for i in range(len(entry_list)):
        # Print the correct box-drawing character
        separator = {
            True: BOTTOM_SEP,
            False: MIDDLE_SEP
        }[i == len(entry_list) - 1]

        print("{}  ".format(VERT_SEP) * tabs +
              separator + 2*BAR_SEP + " " + entry_list[i])

def print_wallet(wallet_id, bus, wallet):
    print("[{}]".format(wallet))
    folders = bus.folderList(wallet_id,
                             "kde-service-menu-nowardev-scanner")
    for folder in folders:
        print_folder(wallet_id, bus, folder, tabs=1)

def get_value(wallet_id, bus, folder_name, key):
    """
    Retrieves the password from the wallet

    Args:
      @wallet_id: The ID of the opened wallet
      @bus: The session bus connection
      @folder_name: The name of the folder the key is in
      @key: The key of the password that should be retrieved

    Returns:
      The value that is stored in the wallet
    """
    value = bus.readPassword(wallet_id,
                             folder_name,
                             key,
                             "kde-service-menu-nowardev-scanner")
    return value

def debug(msg):
    """
    Prints out debug output when the -d or --debug is specified.

    Args:
      @msg: The message to print out
    """
    debug = "-d" in sys.argv or "--debug" in sys.argv
    if debug:
        print("[D] " + msg)

def open_wallet(bus, wallet_name):
    """
    Opens the wallet

    Args:
      @bus: The dbus connection
      @wallet_name: The name of the wallet to open

    Returns:
      The handle of the opened wallet
    """
    debug("Opening wallet \"{}\"".format(wallet_name))
    return bus.open(wallet_name, 0, "kde-service-menu-nowardev-scanner")

def main():
    """The main function"""
    debug("Connecting to the session bus...")
    bus = get_kwalletd5()
    debug("Connected to org.kde.kwalletd5")

    # Should everything be printed
    if len(sys.argv) == 1:
        wallets = bus.wallets()
        for i in range(len(wallets)):
            wallet = wallets[i]
            wallet_id = open_wallet(bus, wallet)
            print_wallet(wallet_id, bus, wallet)
            bus.disconnectApplication(str(wallet_id),
                                      "kde-service-menu-nowardev-scanner")
            # New line just for the style
            if i != len(wallets) - 1:
                print()
    else:
        debug("Path: " + sys.argv[-1])
        args = sys.argv[-1].split("/")
        wallet_id = open_wallet(bus, args[0])

        if len(args) == 2:
            debug("Printing out the folder")
            debug("Wallet ID: " + str(wallet_id))
            debug("Folder name: " + args[1])
            print_folder(wallet_id, bus, args[1])
        elif len(args) == 3:
            print(get_value(wallet_id, bus, args[1], args[2]))
        # Disconnect the script
        bus.disconnectApplication(str(wallet_id),
                                  "kde-service-menu-nowardev-scanner")

if __name__ == "__main__":
    main()
